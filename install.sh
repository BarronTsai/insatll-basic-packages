#!/bin/bash
# Program:
#	Used to install basic packages.
# History:
# 2016/10/05	Barron 	Tsai release

sudo apt-get update
sudo apt-get upgrade -y

export LANGUAGE=en\_US.UTF-8
export LANG=en\_US.UTF-8
export LC\_ALL=en\_US.UTF-8
sudo locale-gen en\_US.UTF-8
sudo dpkg-reconfigure locales

sudo apt-get install -y \
    build-essential \
    vim \
    chromium-browser \
    curl \
    libssl-dev \
    git \
    mercurial \
    pepperflashplugin-nonfree \
    openjdk-7-jre-headless

#Shell 
sudo apt-get install -y zsh
curl -L http://install.ohmyz.sh | sh




